A simple server for Stanford CoreNLP functionality.
Initial version will support text over HTTP for English parts of speech tagging.
More features to follow as I see fit.

IN:
POST text/plain to /v1/posTagging/en

OUT:
text/plain tagged text, or 404

EXAMPLE:
```
$ curl -X POST http://localhost:8080/v1/posTagging/en -d "hello, my name is Matt. I like catfish po'boys."
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   162    0   115  100    47   3709   1516 --:--:-- --:--:-- --:--:--  7187hello__UH ,__, my__PRP$ name__NN is__VBZ Matt__NNP .__.
I__PRP like__VBP catfish__NN po__NN `__`` boys__NNS .__.
```