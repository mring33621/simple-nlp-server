package com.mattring.simple.nlp.server.edu.stanford;

import org.junit.Test;

import java.util.function.Function;

import static org.junit.Assert.assertEquals;

/**
 * Created by MRing on 4/19/2017.
 */
public class TaggingTest {

    @Test
    public void testPOSTaggerEN() throws Exception {
        String doc;
        String taggedDoc;

        doc = "My cat, Ribbons, was stuck in the maple tree. I had to use a ladder to get him down.";
        final Function<String, String> enPosTagger =
                new CoreNLPFunction<>(
                        CoreNLPPipelines.getPOSTaggerEN(),
                        CoreNLPConverters::posTaggedDocToString
                );
        taggedDoc = enPosTagger.apply(doc);
        assertEquals(
                "My__PRP$ cat__NN ,__, Ribbons__NNS ,__, was__VBD stuck__VBN in__IN the__DT maple__NN tree__NN .__. \n" +
                        "I__PRP had__VBD to__TO use__VB a__DT ladder__NN to__TO get__VB him__PRP down__RB .__. \n",
                taggedDoc);
    }

}