package com.mattring.simple.nlp.server.edu.stanford;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.util.CoreMap;

import java.util.List;

/**
 * Created by mring on 5/20/2017.
 */
public class CoreNLPConverters {

    public static String posTaggedDocToString(Annotation annotatedDoc) {
        final List<CoreMap> sentences = annotatedDoc.get(CoreAnnotations.SentencesAnnotation.class);

        final StringBuilder buff = new StringBuilder();
        for (CoreMap sentence : sentences) {
            for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
                String word = token.get(CoreAnnotations.TextAnnotation.class);
                String posTag = token.get(CoreAnnotations.PartOfSpeechAnnotation.class);
                // tag delimiter hardcoded to double underscore
                // TODO: remove hardcoded POS tag delimiter
                buff.append(word).append("__").append(posTag).append(" ");
            }
            buff.append("\n"); // 1 sentence per line
        }

        return buff.toString();
    }


}
