package com.mattring.simple.nlp.server.edu.stanford;

import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

import java.util.Properties;
import java.util.function.Function;

/**
 * Created by MRing on 4/19/2017.
 */
public class CoreNLPFunction<T> implements Function<String, T> {

    final StanfordCoreNLP pipeline;
    final Function<Annotation, T> converter;

    public CoreNLPFunction(Properties props, Function<Annotation, T> converter) {
        this.pipeline = new StanfordCoreNLP(props);
        this.converter = converter;
    }

    protected Annotation annotateDoc(String doc) {
        final Annotation annotatedDoc = new Annotation(doc);
        pipeline.annotate(annotatedDoc);
        return annotatedDoc;
    }

    @Override
    public T apply(String incomingDoc) {
        return converter.apply(annotateDoc(incomingDoc));
    }

}
