package com.mattring.simple.nlp.server.edu.stanford;

import edu.stanford.nlp.util.PropertiesUtils;

import java.util.Properties;

/**
 * Created by mring on 5/20/2017.
 */
public class CoreNLPPipelines {

    public static Properties getPOSTaggerEN() {
        return PropertiesUtils.asProperties(
                "annotators", "tokenize,ssplit,pos",
                "ssplit.isOneSentence", "false",
                "pos.model", "edu/stanford/nlp/models/pos-tagger/english-left3words-distsim.tagger",
                "tokenize.language", "en");
    }


}
