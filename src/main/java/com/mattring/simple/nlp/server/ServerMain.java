package com.mattring.simple.nlp.server;

import com.mattring.simple.nlp.server.edu.stanford.CoreNLPConverters;
import com.mattring.simple.nlp.server.edu.stanford.CoreNLPFunction;
import com.mattring.simple.nlp.server.edu.stanford.CoreNLPPipelines;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import static spark.Spark.*;


/**
 * Created by MRing on 4/19/2017.
 */
public class ServerMain {

    /**
     * default port is 8080
     *
     * @param args [0]=port
     */
    public static void main(String[] args) {

        final int port = args.length == 0 ? 8080 : Integer.parseInt(args[0]);
        port(port);

        final Map<String, Function<String, String>> posTaggers = new HashMap<>();

        final Function<String, String> enPosTagger =
                new CoreNLPFunction<>(
                        CoreNLPPipelines.getPOSTaggerEN(),
                        CoreNLPConverters::posTaggedDocToString
                );
        posTaggers.put("en", enPosTagger);

        post("/v1/posTagging/:lang", (request, response) -> {

            final String lang = request.params("lang");
            final Function<String, String> posTagger = posTaggers.get(lang);

            if (posTagger != null) {
                String doc = request.body();
                String taggedDoc = posTagger.apply(doc);
                response.type("text/plain");
                return taggedDoc;
            } else {
                throw halt(404, "No tagger found for lang=" + lang);
            }

        });
    }

}
